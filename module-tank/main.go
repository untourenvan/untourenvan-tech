package main

import (
	"machine"
	"time"
)

func main() {
	machine.InitADC()

	liter15Pin := machine.ADC{machine.ADC0}
	liter30Pin := machine.ADC{machine.ADC1}
	liter45Pin := machine.ADC{machine.ADC2}
	liter60Pin := machine.ADC{machine.ADC3}
	liter75Pin := machine.ADC{machine.ADC4}

	for {
		liter15Value := liter15Pin.Get()
		liter30Value := liter30Pin.Get()
		liter45Value := liter45Pin.Get()
		liter60Value := liter60Pin.Get()
		liter75Value := liter75Pin.Get()

		liter15 := parseAnalogValue(liter15Value)
		liter30 := parseAnalogValue(liter30Value)
		liter45 := parseAnalogValue(liter45Value)
		liter60 := parseAnalogValue(liter60Value)
		liter75 := parseAnalogValue(liter75Value)

		println(uint16(liter15*255), liter30, liter45, liter60, liter75)

		time.Sleep(time.Millisecond * 500)
	}
}

func parseAnalogValue(inputValue uint16) float32 {
	return float32(inputValue) / float32(0xffff)
}
